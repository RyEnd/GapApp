# GapApp
#### A reservation finder with gap rule implemenation.
 
## How to: Build & Run
#### GapApp is written in java and built with maven.


With Java 8 and Maven installed, clone http://gitlab.com/RyEnd/GapApp.git to a local directory.


#### In the GapApp directory, invoke in the terminal...

    "mvn compile": to compile the code
    
    "mvn install": to install in local repository
    
    "mvn test" or "install": build with tests
    
    
#### To run GapApp
Enter `mvn exec:java -Dexec.mainClass="com.ryanmolnar.gapapp.GapApp"` into terminal