import com.ryanmolnar.gapapp.GapDefier;
import dto.Campsite;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.time.LocalDate;
import java.util.ArrayList;

import org.json.simple.JSONObject;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author ryan
 */
public class GapTest {

  private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
  private final ByteArrayOutputStream errContent = new ByteArrayOutputStream();

  @Before
  public void setUpStreams() {
    System.setOut(new PrintStream(outContent));
    System.setErr(new PrintStream(errContent));
  }

  @After
  public void cleanUpStreams() {
    System.setOut(null);
    System.setErr(null);
  }

  @Test
  public void testReadJSON() {
    String absoluteFilePath = new File("").getAbsolutePath();
    final String FILENAME = "test-case.json";
    String fileLocation = absoluteFilePath + "/" + FILENAME;

    GapDefier tester = new GapDefier();
    ArrayList<String> testSites = new ArrayList();

    String siteOne = "Lewis and Clark Camp Spot";
    String siteTwo = "Davey Crockett Camphouse";
    String siteThree = "Daniel Boone Bungalo";

    testSites.add(siteOne);
    testSites.add(siteTwo);
    testSites.add(siteThree);
    try {
      ArrayList<String> jsonSites = tester.readJSON(fileLocation);
      Assert.assertEquals(testSites, jsonSites);
    } catch (IOException e) {
      System.out.println(e.toString());
      System.out.println("Unable to read file.");
    }
  }

  @Test
  public void testConsoleOut() {
    GapDefier tester = new GapDefier();
    ArrayList<String> stringList = new ArrayList();

    String firstString = "First String";
    String secondString = "Second String";
    
    stringList.add(firstString);
    stringList.add(secondString);

    String testString = "*******Available Campsites*******\n*\n* First String\n* Second String\n*\n*********************************\n";

    tester.printResults(stringList);
    String outputString = outContent.toString();

    Assert.assertEquals(testString, outputString);
  }

  @Test
  public void testCreateCampsite() {
    GapDefier tester = new GapDefier();

    Campsite testSite = new Campsite();
    testSite.setId(1);
    testSite.setName("Test Site");

    JSONObject testJsonSite = new JSONObject();
    testJsonSite.put("id", 1l);
    testJsonSite.put("name", "Test Site");

    Assert.assertEquals(testSite.getId(), tester.createCampsite(testJsonSite).getId());
    Assert.assertEquals(testSite.getName(), tester.createCampsite(testJsonSite).getName());
  }

  @Test
  public void testCheckForOverlap() {
    GapDefier tester = new GapDefier();

    LocalDate startDate = LocalDate.parse("2016-01-01");
    LocalDate endDate = LocalDate.parse("2016-01-03");
    LocalDate resStart = LocalDate.parse("2016-01-04");
    LocalDate resEnd = LocalDate.parse("2016-01-07");
    int gap = 1;
    //search abuts to reservation, no gap
    Assert.assertTrue(tester.checkForOverlap(startDate, resEnd, resStart, endDate, gap));

    resStart = LocalDate.parse("2016-01-05");
    resEnd = LocalDate.parse("2016-01-07");
    //search leaves 1 day gap
    Assert.assertFalse(tester.checkForOverlap(startDate, resEnd, resStart, endDate, gap));

    resStart = LocalDate.parse("2016-01-06");
    resEnd = LocalDate.parse("2016-01-07");
    //search leaves 2 day gap, acceptable relationship to reservation
    Assert.assertTrue(tester.checkForOverlap(startDate, resEnd, resStart, endDate, gap));
  }

  @Test
  public void testFilterSites() {
    GapDefier tester = new GapDefier();
    ArrayList<Campsite> allCampsites = new ArrayList();
    ArrayList<Integer> takenSiteIds = new ArrayList();
    ArrayList<Campsite> testAvailCampsites = new ArrayList();

    Campsite testSite = new Campsite();
    testSite.setId(1);
    testSite.setName("Test Site 1");

    Campsite testSite2 = new Campsite();
    testSite2.setId(2);
    testSite2.setName("Test Site 2");

    Campsite testSite3 = new Campsite();
    testSite3.setId(3);
    testSite3.setName("Test Site 3");

    Campsite testSite4 = new Campsite();
    testSite4.setId(4);
    testSite4.setName("Test Site 4");

    Campsite testSite5 = new Campsite();
    testSite5.setId(5);
    testSite5.setName("Test Site 5");

    allCampsites.add(testSite);
    allCampsites.add(testSite2);
    allCampsites.add(testSite3);
    allCampsites.add(testSite4);
    allCampsites.add(testSite5);

    takenSiteIds.add(1);
    takenSiteIds.add(3);
    takenSiteIds.add(4);

    testAvailCampsites.add(testSite2);
    testAvailCampsites.add(testSite5);

    Assert.assertEquals(testAvailCampsites, tester.filterSites(allCampsites, takenSiteIds));
  }

  // @Test
  // public void differentSearchTests() {
  // }
}
