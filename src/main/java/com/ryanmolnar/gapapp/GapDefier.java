package com.ryanmolnar.gapapp;

import dto.Campsite;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.ListIterator;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

/**
 *
 * @author ryan
 */
public class GapDefier {

  public void run() {
    //json file location in project root
    String absoluteFilePath = new File("").getAbsolutePath();
    final String FILENAME = "test-case.json";
    String fileLocation = absoluteFilePath + "/" + FILENAME;

    try {
      ArrayList<String> availableSites = readJSON(fileLocation);
      printResults(availableSites);
    } catch (IOException e) {
      System.out.println(e.toString());
      System.out.println("Could not find file " + FILENAME);
    }
  }

  public ArrayList<String> readJSON(String filePath) throws IOException {
    JSONParser parser = new JSONParser();
    DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd");

    ArrayList<Campsite> availSites = new ArrayList();
    ArrayList<Integer> freeSite = new ArrayList();
    ArrayList<Integer> takenSites = new ArrayList();
    ArrayList<String> siteNames = new ArrayList();
    Object obj;
    
    //set gap rule to 1 day
    int gap = 1;
    
    try {
      obj = parser.parse(new FileReader(filePath));

      JSONObject jsonObject = (JSONObject) obj;

      //grab new reservation seach dates
      JSONObject search = (JSONObject) jsonObject.get("search");
      LocalDate startDate = LocalDate.parse((String) search.get("startDate"), dtf);
      LocalDate endDate = LocalDate.parse((String) search.get("endDate"), dtf);

      //make each campsite an object for ease of use, add them to a list
      JSONArray allSites = (JSONArray) jsonObject.get("campsites");
      for (int i = 0; i < allSites.size(); i++) {
        JSONObject jsonSite = (JSONObject) allSites.get(i);

        availSites.add(createCampsite(jsonSite));
        freeSite.add((int)(long)jsonSite.get("id"));
      }

      //pull each reservation's info
      JSONArray reservations = (JSONArray) jsonObject.get("reservations");
      for (int i = 0; i < reservations.size(); i++) {
        JSONObject jsonRes = (JSONObject) reservations.get(i);
        LocalDate resStart = LocalDate.parse((String) jsonRes.get("startDate"));
        LocalDate resEnd = LocalDate.parse((String) jsonRes.get("endDate"));
        int campsiteId = (int)(long)jsonRes.get("campsiteId");

        /**
         * check the new search and current reservation for overlap and gap
         * and add site to appropriate list
         */
        if (checkForOverlap(startDate, resEnd, resStart, endDate, gap)) {
          if (!freeSite.contains(campsiteId)) {
            freeSite.add(campsiteId);
          }
        } else if (!takenSites.contains(campsiteId)) {
          takenSites.add(campsiteId);
        }
      }
    } catch (Exception e) {
      System.out.println(e.toString());
      System.out.println("Could not read file.");
    }
    
    //all the sites are then filtered by the taken or inelligble sites
    availSites = filterSites(availSites, takenSites);
    //site names extracted and sent out for printing
    availSites.stream().forEach((campsite) -> {
      siteNames.add(campsite.getName());
    });
    return siteNames;
  }

  public void printResults(ArrayList<String> freeSites) {
    System.out.println("*******Available Campsites*******");
    System.out.println("*");
    freeSites.stream().forEach((freeSite) -> {
      System.out.println("* " + freeSite);
    });
    System.out.println("*");
    System.out.println("*********************************");
  }

  public Campsite createCampsite(JSONObject jsonCampsite) {
    Campsite site = new Campsite();
    site.setId((int) (long) jsonCampsite.get("id"));
    site.setName((String) jsonCampsite.get("name"));
    return site;
  }

  public boolean checkForOverlap(LocalDate startDate, LocalDate resEnd, LocalDate resStart, LocalDate endDate, int gap) {
    //check the difference in new search and curr res dates
    long startsAfter = startDate.compareTo(resEnd);
    long endsBefore = resStart.compareTo(endDate);

    /**
     * account for reservation overlap and gap rule. Assumes a new reservation
     * which either starts after the end or ends before the beginning of the old
     * reservation, accounting for gaps, will fit, and if it fails that
     * criteria, it will not fit.
     */
    return (startsAfter > gap + 1 || startsAfter == 1) || (endsBefore > gap + 1 || endsBefore == 1);
  }

  public ArrayList<Campsite> filterSites(ArrayList<Campsite> availSites, ArrayList<Integer> takenSites) {
    takenSites.stream().forEach((takenSite) -> {
      ListIterator<Campsite> iter = availSites.listIterator();
      while (iter.hasNext()) {
        if (iter.next().getId() == takenSite) {
          iter.remove();
        }
      }
    });
    return availSites;
  }
}
